#!/usr/bin/python3

"""Generate a stacked graph of the Debian upgrade progression."""

import pandas as pd
import matplotlib.pyplot as plt

# Load the data
df = pd.read_csv('subset.csv')

# Pivot the data to a suitable format for plotting
df = df.pivot_table(index="Date", columns='release', values='count', aggfunc='sum')

# Convert the index to datetime and sort it
df.index = pd.to_datetime(df.index)

print(df)

# Plotting the data with filled areas
fig, ax = plt.subplots(figsize=(12, 6))
df.plot(ax=ax, kind="area", stacked=True, alpha=0.7, linewidth=0)

plt.show()
