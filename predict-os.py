#!/usr/bin/python3
# coding: utf-8

"""predict when major upgrades will complete"""
# Copyright (C) 2016 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import argparse
import collections
from datetime import datetime
import io
import logging
import logging.handlers
import os
import os.path
import sys
import tempfile

try:
    import pytest
except ImportError:  # pragma: no cover
    pytest = None

import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import requests
import seaborn as sns


__epilog__ = """The "predict" command displays the planned upgrade date on
stdout. The "graph" command plots the values in a graph. The "refresh"
command fetches the data from PuppetDB and writes it to the CSV
file. This project is a rewrite of this R toolset in Python:
https://gitlab.com/anarcat/predict-os and expects the following Python
packages to be installed: python3-requests python3-seaborn"""

# the reason this was rewritten in Python was that:
#
# 1. libreoffice is a catastrophe, see the original predict-os for details
# 2. i don't want to learn how to read/write/parse CSV files in R
# 3. i don't want to learn how to make R talk with PuppetDB
# 4. i got tired of chasing the PuppetDB SQL database changes
# 5. i had to use python to massage data anyways
# 6. "code without tests is legacy code" and i don't want legacy code

PUPPETDB_URL = "http://localhost:8080/pdb/query/v4"
PUPPETDB_QUERY = 'facts[value] { name = "lsbdistcodename" }'

DEFAULT_HEADER = ["Date", "release", "count"]

DEFAULT_OLDSTABLE = "stretch"


def parse_args(args=sys.argv[1:]):
    parser = argparse.ArgumentParser(description=__doc__, epilog=__epilog__)
    parser.add_argument(
        "--verbose",
        "-v",
        dest="log_level",
        action="store_const",
        const="info",
        default="warning",
    )
    parser.add_argument(
        "--debug",
        "-d",
        dest="log_level",
        action="store_const",
        const="debug",
        default="warning",
    )
    parser.add_argument(
        "--test", action="store_true", help="run self test suite and nothing else"
    )
    parser.add_argument(
        "--dryrun", "-n", action="store_true", help="do not write anything permanently"
    )
    parser.add_argument(
        "--puppetdb",
        "-p",
        default=PUPPETDB_URL,
        help="PuppetDB server URL, default: %(default)s",
    )
    parser.add_argument(
        "--query",
        default=PUPPETDB_QUERY,
        help="query returning the list of Debian releases, default: %(default)s",
    )
    parser.add_argument(
        "--path",
        default="data.csv",
        help="CSV datafile that keeps past records, default: %(default)s",
    )
    parser.add_argument(
        "--output",
        "-o",
        type=argparse.FileType("wb"),
        default=None,
        const="predict-%s.png" % DEFAULT_OLDSTABLE,
        nargs="?",
        help="write to an image file (default: %(const)s)",
    )
    parser.add_argument(
        "--plot-kind",
        choices=("lm", "line", "bar", "area"),
        default="lm",
        help="which plot type to draw, default: %(default)s",
    )
    parser.add_argument(
        "--source",
        "-s",
        default=DEFAULT_OLDSTABLE,
        help="major version we are upgrading from, default: %(default)s",
    )

    parser.add_argument(
        "action",
        choices="predict graph refresh".split(),
        nargs="*",
        default="predict",
        help='action to take, default is "%(default)s"',
    )

    return parser.parse_args(args=args)


def main(args, now=None, session=requests):
    logging.debug("loading previous records from %s", args.path)
    if "refresh" in args.action and not args.dryrun and not os.path.exists(args.path):
        with open(args.path, "w") as fp:
            fp.write(",".join(DEFAULT_HEADER))
    with open(args.path) as fp:
        records = load_csv(fp)
    if "refresh" in args.action:
        logging.info("querying PuppetDB on %s", args.puppetdb)
        logging.debug("query: %s", args.query)
        new_data = puppetdb_query(args.puppetdb, args.query, session)
        logging.info("found %d hosts", len(new_data))
        new_record = count_releases(new_data)
        records = add_releases(records, new_record)
        if not args.dryrun:
            with open(args.path, "w") as fp:
                store_csv(fp, records)
    records = prepare_records(records)
    date = None
    if "predict" in args.action:
        date = guess_completion_time(records, args.source, now)
        print("completion time of %s major upgrades: %s" % (args.source, date))
    if "graph" in args.action:
        plot_records(records, date, args)


def test_main(capsys):
    test_input = b"""Date,release,count
2019-03-01,stretch,74
2019-08-15,stretch,49
2019-10-07,stretch,43
2019-10-08,stretch,38
"""
    expected = """completion time of stretch major upgrades: 2020-06-25"""
    with tempfile.NamedTemporaryFile() as csv:
        csv.write(test_input)
        csv.flush()
        with tempfile.NamedTemporaryFile(suffix=".png") as graph:
            args = parse_args(["--path", csv.name, "--output", graph.name])
            main(args, "2019-10-08")
            assert os.path.getsize(graph.name) > 0
        captured = capsys.readouterr()
        assert expected in (captured.out + captured.err)


def test_main_refresh():
    import betamax

    session = requests.Session()
    recorder = betamax.Betamax(session, cassette_library_dir="cassettes")
    handler = _setup_memory_handler()
    with tempfile.NamedTemporaryFile() as csv:
        os.unlink(csv.name)
        with tempfile.NamedTemporaryFile(suffix=".png") as graph:
            args = parse_args(["--path", csv.name, "--output", graph.name, "--refresh"])
            with recorder.use_cassette("puppetdb"):
                main(args, "2019-10-08", session)
            assert os.path.exists(csv.name)
            with open(csv.name) as fp:
                assert (
                    fp.read()
                    == """Date,release,count
2019-10-09,buster,40
2019-10-09,stretch,38
2019-10-09,jessie,1
"""
                )
            messages = "\n".join([r.message for r in handler.buffer])
            assert """cannot guess completion time""" in messages


def load_csv(fp):
    """load the data from the CSV"""
    return pd.read_csv(fp)


SAMPLE_CSV = """Date,release,count
2019-01-01,buster,32
2019-01-01,stretch,10
2019-02-02,buster,37
2019-02-02,stretch,5
2019-03-03,buster,50
2019-03-03,stretch,1
"""

SAMPLE_DF_REPR = """         Date  release  count
0  2019-01-01   buster     32
1  2019-01-01  stretch     10
2  2019-02-02   buster     37
3  2019-02-02  stretch      5
4  2019-03-03   buster     50
5  2019-03-03  stretch      1"""


def test_load_csv():
    """just a sanity check that pandas works as expected"""
    fp = io.StringIO(SAMPLE_CSV)
    res = load_csv(fp)
    assert repr(res) == SAMPLE_DF_REPR
    return res


def store_csv(fp, records):
    """write the CSV file back to the given stream"""
    return fp.write(records.to_csv(index=False))


def test_store_csv():
    """just a sanity check that we do the CSV rountrip cleanly"""
    fp = io.StringIO()
    data = test_load_csv()
    store_csv(fp, data)
    fp.seek(0)
    assert fp.read() == SAMPLE_CSV


def puppetdb_query(url, query, session=requests):
    """get the data from PuppetDB"""
    resp = session.get(url, data={"query": query})
    resp.raise_for_status()
    return resp.json()


def test_puppetdb_query():
    """simulate a PuppetDB query"""
    import betamax

    session = requests.Session()
    recorder = betamax.Betamax(session, cassette_library_dir="cassettes")
    with recorder.use_cassette("puppetdb"):
        json = puppetdb_query(PUPPETDB_URL, PUPPETDB_QUERY, session=session)
    assert len(json) > 0
    return json


def count_releases(data):
    """parse the data returned by PuppetDB

    This counts the number of entries for each releases.

    >>> d = [{'value': 'buster'}, {'value': 'stretch'}, {'value': 'buster'}]
    >>> count_releases(d)
    {'buster': 2, 'stretch': 1}
    """
    total = collections.defaultdict(int)
    for item in data:
        logging.debug("checking item %s", item)
        total[item["value"]] += 1
    return dict(total)


def add_releases(data, new_data, date=None):
    """take the existing data and appending the new record"""
    if date is None:
        date = datetime.today().strftime("%Y-%m-%d")
    series = [
        {"Date": date, "release": release, "count": count}
        for release, count in new_data.items()
    ]
    return pd.concat([data, pd.DataFrame(series)], ignore_index=True)


def test_add_releases():
    """check that we can add to the pandas dataframe as expected"""
    data = test_load_csv()
    new_data = {"buster": 33, "stretch": 9}
    d = add_releases(data, new_data, "2019-04-05")
    assert (
        SAMPLE_DF_REPR
        + """
6  2019-04-05   buster     33
7  2019-04-05  stretch      9"""
        == repr(d)
    )


# cargo-culted from https://stackoverflow.com/questions/48860428/passing-datetime-like-object-to-seaborn-lmplot  # noqa: E501
@plt.FuncFormatter
def fake_dates(x, pos):
    """Custom formater to turn floats into e.g., 2016-05-08"""
    # normally, this should work:
    # ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%Y-%m-%d'))
    # ... but it doesn't seem to like how seaborn treats dates
    return matplotlib.dates.num2date(x).strftime("%Y-%m-%d")


def plot_records(records, guessed_date, args):
    """draw the actual graph, on the GUI or in a file as args dictates"""
    sns.set(color_codes=True)
    if args.plot_kind == 'lm':
        # ci=False because it looks kind of wrong
        graph = sns.lmplot(x="datenum", y="count", hue="release", data=records, ci=False)
        # fix weird API inconsistency between lmplot and lineplot
        ax = graph.ax
    else:
        # we ended up using matplotlib for everything as, the sns
        # barplot is just to damn ugly and the histplot just doesn't
        # work, i have tried:
        #
        # ax = graph = sns.histplot(x="datenum", y="count", hue="release", data=records)
        stacked = args.plot_kind != 'line'
        df = records.pivot_table(index="Date", columns='release', values='count', aggfunc='sum')
        logging.debug("df: %s", df)
        linewidth = None  # this tells mpl to use the default
        if args.plot_kind == 'area':
            linewidth = 0
        ax = df.plot(kind=args.plot_kind, stacked=stacked, linewidth=linewidth, figsize=(16,9))
        if args.plot_kind == 'bar':
            logging.warning("note that the bar graph's X axis is not scaled properly over time")

    # return numeric dates into human-readable
    if args.plot_kind == 'lm':
        # SNS needs more work to display dates properly
        ax.xaxis.set_major_formatter(fake_dates)
        # labels overlap otherwise
        ax.tick_params(labelrotation=45)
    if guessed_date is None:
        title = "Debian major upgrades from %s progress" % args.source
    else:
        title = "Debian major upgrades from %s planned completion by %s" % (
            args.source,
            guessed_date,
        )
    ax.set_title(title)
    ax.set_xlabel("date")

    if args.dryrun or (
        args.output is None and (sys.stdout.isatty() or "DISPLAY" in os.environ)
    ):
        plt.show()  # pragma: no cover
    else:
        _, ext = os.path.splitext(args.output.name)
        plt.savefig(args.output, format=ext[1:], bbox_inches="tight")


def prepare_records(records):
    """various massaging required by other tools

    This currently only stores the numeric date for seaborn and
    regression processing.
    """
    records["datenum"] = matplotlib.dates.datestr2num(records["Date"])
    return records


def guess_completion_time(records, source, now=None):
    """take the given records and guess the estimated completion time

    :param Dataframe records: the records, as loaded from the CSV file
           by load_csv)
    :param str source: the kind of `release`. will fail if unknown
    :returns: completion date, formatted as a string (YYYY-MM-DD)

    >>> records = prepare_records(test_load_csv())
    >>> guess_completion_time(records, 'stretch')
    '2019-03-09'
    """
    if now is None:
        now = datetime.today().strftime("%Y-%m-%d")
    subdf = records[records["release"] == source]
    try:
        fit = np.polyfit(subdf["count"], subdf["datenum"], 1)
        prediction = np.poly1d(fit)(0)
    except (TypeError, ValueError) as e:
        logging.warning("cannot guess completion time: %s", e)
        date = "N/A"
    else:
        date = fake_dates(prediction, None)
        if date < now:
            logging.warning(
                "suspicious completion time in the past, data may be incomplete: %s",
                date,
            )
    return date


def test_guess_completion_time():
    test_input = """Date,release,count
2019-03-01,stretch,74
2019-08-15,stretch,49
2019-10-07,stretch,43
2019-10-08,stretch,38
"""
    expected = """2020-06-25"""
    fp = io.StringIO(test_input)
    records = prepare_records(pd.read_csv(fp))
    date = guess_completion_time(records, "stretch", "2019-10-08")
    assert date == expected


def _setup_memory_handler():
    handler = logging.handlers.MemoryHandler(1000)
    handler.setLevel("DEBUG")
    logger = logging.getLogger("")
    logger.setLevel("DEBUG")
    logger.addHandler(handler)
    return handler


def test_weird_completion_time(capsys):
    micah_data = """Date,release,count
2019-10-08,stretch,83
2019-10-08,buster,3
2019-10-08,sid,1
2019-10-08,jessie,2"""
    fp = io.StringIO(micah_data)
    records = prepare_records(pd.read_csv(fp))
    with pytest.warns(np.RankWarning):
        handler = _setup_memory_handler()
        guess_completion_time(records, "stretch", "2019-10-08")
        messages = "\n".join([r.message for r in handler.buffer])
        assert "suspicious completion time in the past" in messages


if __name__ == "__main__":  # pragma: no cover
    args = parse_args()
    logging.basicConfig(format="%(message)s", level=args.log_level.upper())
    if args.test:
        logging.info("running test suite")
        if pytest is None:
            logging.error("test suite requires pytest to run properly")
            sys.exit(1)
        shortname, _ = os.path.splitext(os.path.basename(__file__))
        sys.exit(pytest.main(["--doctest-modules", "--cov=%s" % shortname, __file__]))
    try:
        main(args)
    except Exception as e:
        logging.error("unexpected error: %s", e)
        if args.log_level == "debug":
            logging.warning('starting debugger, type "c" and enter to continue')
            import traceback
            import pdb
            import sys

            traceback.print_exc()
            pdb.post_mortem()
            sys.exit(1)
        raise e
