Major OS upgrade prediction tools
=================================

This program allows you to extract reports from a [Puppet][][DB][]
and, over time, analyse trends in the number of machines running
different operating system releases, and predict when major upgrades
will be completed across a fleet of servers.

[Puppet]: https://puppet.com/
[DB]: https://puppet.com/docs/puppetdb/

Usage
=====

Run this on the PuppetDB server:

    ./predict-os.py --refresh -o /dev/null

This will create a CSV file with today's results. Ignore the warnings
about being unable to do a prediction, it doesn't have enough data
yet.

Then, in the coming week, do some upgrades and run the command again
to generate a prediction and graphs:

    ./predict-os.py --refresh -o graph.png

This will show a prediction date, for example:

    completion time of stretch major upgrades: 2020-05-16

You can run this on your own workstation and show an interactive graph
by forwarding a local port to the PuppetDB server first:

    ssh -N -L8080:localhost:8080 puppetdb.example.com &

Then you run the command normally on your workstation.

See also the detailed usage in `--help`.

Contributing and support
========================

See the `CONTRIBUTING.md` document for more information.

History
=======

This was first done with a [LibreOficce][] spreadsheet, but it turned
out to be ridiculously difficult to do linear regressions in that
tool. It was also difficult to automatically (ie. non-interactively)
perform updates to the data and extract the graph's image out of the
file.

This approach was therefore abandoned in favor of [R][] but that was
also difficult: it require a lot of glue around it to extract the
results from Puppet, parse them into something R would understand, and
generate the graphs with R. It ended up being a smoking pile of R,
Make and Python, which was not pretty to look at, and hard to
maintain. It was then rewritten from scratch in Python.

This project was written by Antoine Beaupré <anarcat@debian.org>,
first for [Koumbit][] between 2014 and 2016, and then rewritten for
the [Tor Project][] in 2019. The old code before the rewrite is
available on the `0.1` tag in the project history.

[Tor Project]: https://www.torproject.org/
[Koumbit]: https://www.koumbit.org/
[R]: https://www.r-project.org/
[LibreOficce]: https://www.libreoffice.org/
