0.2.0 / 2019-10-23
==================

  * rewrite everything in a single python script
  * add contributing instructions, copied from wallabako
  * show all defaults properly
  * rewrite README in english and update for new code

0.1.0
=====

Last release of the pile of R, Libreoffice, Make and Python code.
